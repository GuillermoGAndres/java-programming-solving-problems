# Todo lo que tines que saber sobre la varible CLASSPATH y archivos jar
variableCLASSPATHyarchivosJAR

Si tienes una libreria externa, en nuestro edu.duke
en ella cotendra todo los archivo classs que vamos ocupar en nuestro programas cuando
escribamos la sentencia
        import edu.duke.\*
Cuando tratemos de ejecutar nuestro de compilar nuestro programa, va generar
un error como este  _HelloWorld.java:1: error: package edu.duke does not exist_ , 
el cual significa que no conoce donde encontrar nuestro paquetea y las clases
que vamos utilzar en nuestro codigo.

Esto se debe porque cuando compilamos con `javac` lo que es buscar, las clases
definidas en un cierta libreria, sino me equibo /lib/ donde estan definidas las
clases por defecto que viene java como import.util.xxx
Y despues checa en la variable de entorno CLASSPATH, para ver si esta ahi definidas
las clases. Muy pareceido a C/C++ cuando compilas que tiene que revisar en el   donde esta las libreria estandar.

Para solucionar esto, nosotros tenemos que indicarle al compilador donde esta defina nuestra libreria que contiene nuestras clases, para hacer esto debemo crear una varible de entorno llamada CLASSPATH la cual no esta definida por el momento, una definida con la ubicacion de nuestra libreria, automaticamente el compilador de java(javac) sabe que nuestro de buscar en la ruta por defecto, buscar ahora en la variable CLASSPATH:
## Codigo para crear la variable CLASSPATH y agregar nuestra ruta de nuestra libreia en Linux.
~~~
$ CLASSPATH=.

$ CLASSPATH=/home/usuario/archivo.jar

$ export CLASSPATH

~~~
@see:<http://www.chuidiang.org/java/novatos/hola_mundo_java.php#classpath>

El archivo punto jar es donde estan empaquetadas todas tus clases.
Ahora, nuestra variable de entorno sera global y el compilador podra ir a revisarla,
pero esto solo durara en nuestra sesion, una vez acaba,es decir, cerrando nuestra terminal
volvera, a sus valores normales de la variable de entono, tendriamos que volver hacer el mismo
procedimiento.

Si deseas que la configuración se mantenga en todos los inicios de sesión, debes definir las variables de entorno en tu archivo de inicialización personal, es decir .bash\_profile o ya sea si lo prefieres en .bashrc, estos archivos son los que se ejecutan automaticamente cuando abres tu terminal al inicio.
@see: <https://www.hostinger.mx/tutoriales/variables-de-entorno-linux-como-leerlas-y-configurarlas-vps/>

Ver sobre jar
<http://www.chuidiang.org/java/novatos/hacer_ficheros_jar.php>
Ver sobre CLASSPATH
<http://www.chuidiang.org/java/novatos/hola_mundo_java.php#classpath>
<http://www.chuidiang.org/java/classpath/classpath.php>

---
# Para que se utiliza la variable de entorno CLASSPATH

Uno de los propositos por el cual tengamos que utilizar la variable CLASSPATH esta muy relacionado con los paquete jar de Java.
Una vez que tengamos nuestras clases enpaquetacadas en fichero Jar, para poder ser utilzadas bastara con tener incluido el fichero Jar en el CLASSPATH.
Esa uno de los propositos por los cuales utilzaras esa variable, dado un ficheroJar con clases que quieres ejecutar.
Ya sea creando una variable de enterno CLASSPATH=/HOME/fichero jar export CLASSPATH o utilzando la linea de comandos
        java -cp /HOME/fichero.jar ClasePrincipalDelJar

Todo haciendo referencia con la Clase princial(Main) que contiene el jar a ejecutar, tambien puede hacerse de otra forma, si tenemos definidos nuestro Manifest,donde definimos nuestra clase principal, se puede ejecutar directamente el jar:
~~~
java -jar ficheroJar
~~~
Atributos del comando jar

@See: <http://www.jtech.ua.es/j2ee/2002-2003/modulos/java/apuntes/apuntes5.htm>

\(MUY BUEN DOCUMENTO PARA GENERAR Y EXTRAER PUNTO JAR\)
---
Acercade paquetes
@see: <http://www.chuidiang.org/java/novatos/hola_mundo_paquetes.php>

---
### Forma de descomprimir los jar 
~~~
jar -xvf Mijar.jar 
~~~

-x extract \( Extraer \)

-v verbose \(No da mas informacion al momento de descomprimirlo en la terminal\)

-f filename \(Nombre del jar \)

