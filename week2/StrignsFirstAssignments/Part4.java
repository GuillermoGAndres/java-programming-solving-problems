import edu.duke.*;
/**
 *Write a program that reads the lines from the file at this URL location, http://www.dukelearntoprogram.com/course2/data/manylinks.html, and prints each URL on the page that is a link to youtube.com.
 * */
public class Part4{

    public static void main(String[] args){
        URLResource url = new URLResource("https://www.dukelearntoprogram.com//course2/data/manylinks.html");
        // for(String line : url.lines()){
        //     System.out.println(line);
        // }
        // // String content = url.asString();
        // System.out.println(content);
        for(String word : url.words()){
            //System.out.println(word);
            String lowerWord = word.toLowerCase();
            int youtube = lowerWord.indexOf("youtube.com");
            if( youtube > -1 ){
                int beginlink = lowerWord.lastIndexOf("\"", youtube);
                int endlink = lowerWord.indexOf("\"", youtube);
                System.out.println( word.substring(beginlink, endlink+1)  );
            }


        }

    }
}


