import edu.duke.*;

public class Part1{
   
    public String findSimpleGene(String dna){
        int index = dna.indexOf("ATG");
        int indexEnd;   
        String substring;
        if( index > -1){
            indexEnd = dna.indexOf("TAA", index+3); 
            if(indexEnd > -1){
                 //Evaluar ahora que sea multiplo de 3
                substring = dna.substring(index, indexEnd+3);
                System.out.println(substring);
                if( substring.length() % 3 == 0 )
                    return substring;

            }
        }
        return "";
    }

    public void testSimpleGene(){
        String dna1 = "AERSDAFCDASAFAD";
        String dna2 = "ADFASDFAAFATAA" ;
        String dna3 = "ERFSATGSDFGSDGSD";
        String dna4 = "ADFATGASDFADTAADF";
        String dna5 = "ASFDATDRERTAATRY";
        String[] tests = {dna1, dna2, dna3, dna4, dna5};
        int i=1;
        for(String test :  tests ){
            System.out.println("Test " + i);
            System.out.println(findSimpleGene(test));
            i++;
        }
        String b = "AAATGCCCTAACTAGATTAAGAAACC";
        System.out.println("Response: \n" + findSimpleGene(b));
    }


    public static void main(String[] args){
        System.out.println("Hola mundo");
        new Part1().testSimpleGene();
    }
    
}
