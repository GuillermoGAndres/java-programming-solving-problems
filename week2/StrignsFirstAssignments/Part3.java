//import edu.duke.*;

public class Part3{
    
    /**
     * 
     * @param a
     * @param b
     * @return This method returns true if string a appears at least twice in string b, otherwise it returns false.
     */
    public boolean twoOcurrences(String a, String b){
        int first = b.indexOf(a);
        System.out.println(first);
        int seconds = -1;
        if(first != -1){
            seconds = b.indexOf(a, first+1);
            System.out.println(seconds);
        }
        return first > -1 && seconds > -1;
    }
    /*. This method finds the first occurrence of stringa in stringb, and returns the part of stringb that follows stringa. If stringa does not occur in stringb, then return stringb.
     */
    public String lastPart(String a, String b){
        int first = b.indexOf("a");
        if(first > -1)
            return b.substring(first + a.length(), b.length());
        return b;
    }

    public void testing(){
        System.out.println(new Part3().twoOcurrences("atg","ctgtatgta"));
          System.out.println(new Part3().lastPart("an","banana"));
          System.out.println(new Part3().lastPart("zoom","forest")); 
    }
   
    public String findSimpleGene(String dna, String startCodon,String stopCodon){
        boolean isLower = false;
        //Verificamos si es mayusculas o minusculas
        if( dna.equals(dna.toLowerCase()) ){
            //Entrara si las dos son minusculas
            isLower = true;
        }
        int index = dna.indexOf(startCodon);
        int indexEnd;   
        String substring;
        if( index > -1){
            indexEnd = dna.indexOf(stopCodon, index+3); 
            if(indexEnd > -1){
                 //Evaluar ahora que sea multiplo de 3
                substring = dna.substring(index, indexEnd+3);
                //System.out.println(substring);
                if( substring.length() % 3 == 0 ){
                    if( isLower )
                        return substring.toLowerCase();
                    return substring.toUpperCase();
                }
            }
        }
        return "";
    }

    public void testSimpleGene(){
        String dna1 = "AERSDAFCDASAFAD";
        String dna2 = "ADFASDFAAFATAA" ;
        String dna3 = "ERFSATGSDFGSDGSD";
        String dna4 = "ADFATGASDFADTAADF";
        String dna5 = "ASFDATDRERTAATRY";
        String[] tests = {dna1, dna2, dna3, dna4, dna5};
        int i=1;
        for(String test :  tests ){
            System.out.println("Test " + i);
            System.out.println(findSimpleGene(test,"ATG","TAA"));
            i++;
        }
        
    }


    public static void main(String[] args){
        // System.out.println("Hola mundo");
        // new Part2().testSimpleGene();
        new Part3().testing();
    }
    
}
