//import edu.duke.*;

public class Part2{
   
    public String findSimpleGene(String dna, String startCodon,String stopCodon){
        boolean isLower = false;
        //Verificamos si es mayusculas o minusculas
        if( dna.equals(dna.toLowerCase()) ){
            //Entrara si las dos son minusculas
            isLower = true;
        }
        int index = dna.indexOf(startCodon);
        int indexEnd;   
        String substring;
        if( index > -1){
            indexEnd = dna.indexOf(stopCodon, index+3); 
            if(indexEnd > -1){
                 //Evaluar ahora que sea multiplo de 3
                substring = dna.substring(index, indexEnd+3);
                //System.out.println(substring);
                if( substring.length() % 3 == 0 ){
                    if( isLower )
                        return substring.toLowerCase();
                    return substring.toUpperCase();
                }
            }
        }
        return "";
    }

    public void testSimpleGene(){
        String dna1 = "AERSDAFCDASAFAD";
        String dna2 = "ADFASDFAAFATAA" ;
        String dna3 = "ERFSATGSDFGSDGSD";
        String dna4 = "ADFATGASDFADTAADF";
        String dna5 = "ASFDATDRERTAATRY";
        String[] tests = {dna1, dna2, dna3, dna4, dna5};
        int i=1;
        for(String test :  tests ){
            System.out.println("Test " + i);
            System.out.println(findSimpleGene(test,"ATG","TAA"));
            i++;
        }
        
    }


    public static void main(String[] args){
        System.out.println("Hola mundo");
        new Part2().testSimpleGene();
    }
    
}
