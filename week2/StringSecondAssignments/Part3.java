
public class Part3{

/**
 * 
 * @param dna
 * @param startIndex
 * @param stopCodon
 * @return the index of the first occurrence of stopCodon that appears past startIndex and is a multiple of 3 away from startIndex. If there is no such stopCodon, this method returns the length of the dna strand.
 */
    public int findStopCodon(String dna, int startIndex, String stopCodon){
        int currIndex = dna.indexOf(stopCodon, startIndex+3);
        int diff = currIndex - startIndex;
        if( currIndex != -1 &&  diff % 3 == 0)
            return currIndex;
        return dna.length();
    }

    public void testFindStopCodon(){ }

    public String fineGene(String dna, int where){
        int startIndex = dna.indexOf("ATG", where);
        if(startIndex == -1)
            return "";
        int taaIndex = findStopCodon(dna, startIndex, "TAA");
        int tagIndex = findStopCodon(dna, startIndex, "TAG");
        int tgaIndex = findStopCodon(dna, startIndex, "TGA");
        int minStop;
        if(taaIndex == -1 || ( taaIndex != -1 && tagIndex < taaIndex  )){
            minStop = tagIndex;
        }else{
            minStop = taaIndex;
        }
        if( minStop == -1 || (tgaIndex != -1 && tgaIndex < minStop)){
            minStop = tgaIndex;
        }
        if( tgaIndex == -1)
            return "";
        return dna.substring(startIndex, minStop);
    }

    public void testFineGene(){}

    public void printAllGenes(String dna){
       int startIndex=0;
       while(true){
           String currentGen = fineGene(dna, startIndex);
           if(  currentGen.isEmpty())
               break;
           System.out.println(currentGen);
           startIndex = dna.indexOf(currentGen,startIndex) + currentGen.length();
       }
    }
    public int countGenes(String dna){
        int cout=0;
        int startIndex=0;
        while(true){
            String currentGen = fineGene(dna, startIndex);
            if(currentGen.isEmpty())
                break;
            cout++;
            startIndex = dna.indexOf(currentGen, startIndex) + currentGen.length();
        }
        return cout;

    }
    public void testCountGenes(){
        System.out.println(countGenes("ATGTAAGATGCCCTAGT"));
    }
    public static void main(String[] args){
        new Part3().testCountGenes();

    }
}
