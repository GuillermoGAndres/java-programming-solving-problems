
public class Part1{

/**
 * 
 * @param dna
 * @param startIndex
 * @param stopCodon
 * @return the index of the first occurrence of stopCodon that appears past startIndex and is a multiple of 3 away from startIndex. If there is no such stopCodon, this method returns the length of the dna strand.
 */
    public int findStopCodon(String dna, int startIndex, String stopCodon){
        int currIndex = dna.indexOf(stopCodon, startIndex+3);
        int diff = currIndex - startIndex;
        if( currIndex != -1 &&  diff % 3 == 0)
            return currIndex;
        return dna.length();
    }

    public void testFindStopCodon(){ }

    public String findGene(String dna){
        int startIndex = dna.indexOf("ATG");
        if(startIndex == -1)
            return "";
        int taaIndex = findStopCodon(dna, startIndex, "TAA");
        int tagIndex = findStopCodon(dna, startIndex, "TAG");
        int tgaIndex = findStopCodon(dna, startIndex, "TGA");
        int minStop;
        if(taaIndex == -1 || ( taaIndex != -1 && tagIndex < taaIndex  )){
            minStop = tagIndex;
        }else{
            minStop = taaIndex;
        }
        if( minStop == -1 || (tgaIndex != -1 && tgaIndex < minStop)){
            minStop = tgaIndex;
        }
        if( tgaIndex == -1)
            return "";
        return dna.substring(startIndex, minStop);
    }

    public void testFineGene(){
        
        System.out.println(findGene("AATGCTAACTAGCTGACTAAT") );
    }

    public void printAllGenes(String dna){
        
    }
    public static void main(String[] args){
        new Part1().testFineGene();

    }
}
