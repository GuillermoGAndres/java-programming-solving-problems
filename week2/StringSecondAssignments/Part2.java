


public class Part2{

	public int howMany(String stringa, String stringb){
        int cout=0;
        int startIndex = stringb.indexOf(stringa);
        if(startIndex == -1)
            return cout;
        while(startIndex > -1){
            cout++;
            startIndex = stringb.indexOf(stringa, startIndex+stringa.length());
        }
        return cout;
	}

    public void testHowMany(){
        System.out.println( howMany("GAA", "ATGAACGAATTGAATC") ); //return 3
        System.out.println( howMany("AA", "ATAAAA") ); // return 2
    }

	public static void main(String[] args){
        new Part2().testHowMany();

	}
}
